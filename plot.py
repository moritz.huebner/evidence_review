import numpy as np
import scipy
import matplotlib
from matplotlib import pyplot as plt
from scipy.stats import binom, norm
import bilby
from bilby.core.utils import logger


@bilby.core.utils.latex_plot_format
def evidence_pp_plot(results, analytical_log_evidence, confidence_interval=0.95,
                     filename='', legend_fontsize='x-small', title=True,
                     confidence_interval_alpha=0.1, **kwargs):

    credible_levels = []
    for result in results:
        credible_levels.append(get_gaussian_percentile(x=result.log_evidence, mu=analytical_log_evidence,
                                                       sigma=result.log_evidence_err))
    x_values = np.linspace(0, 1, 1001)
    fig, ax = plt.subplots()
    number_of_results = len(credible_levels)

    edge_of_bound = (1. - confidence_interval) / 2.
    lower = binom.ppf(1 - edge_of_bound, number_of_results, x_values) / number_of_results
    upper = binom.ppf(edge_of_bound, number_of_results, x_values) / number_of_results
    lower[0] = 0
    upper[0] = 0
    ax.fill_between(x_values, lower, upper, alpha=confidence_interval_alpha, color='k')

    pp = np.array([sum(credible_levels < xx) /
                   len(credible_levels) for xx in x_values])
    pvalue = scipy.stats.kstest(credible_levels, 'uniform').pvalue
    logger.info("KS-test p-value: {}".format(pvalue))
    label = "P value ({:2.3f})".format(pvalue)

    plt.plot(x_values, pp, label=label, **kwargs)

    if title:
        ax.set_title("N={}, p-value={:2.4f}".format(len(results), pvalue))
    ax.set_xlabel("C.I.")
    ax.set_ylabel("Fraction of events in C.I.")
    ax.legend(linewidth=1, handlelength=2, labelspacing=0.25, fontsize=legend_fontsize)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    fig.tight_layout()
    fig.savefig('{}_pp.pdf'.format(filename), dpi=500)
    plt.clf()
    return pp

@bilby.core.utils.latex_plot_format
def multiple_evidence_pp_plot(pps, nlives, sampler, covariance, modality, confidence_interval=0.90,
                              filename='', legend_fontsize='medium',
                              confidence_interval_alpha=0.1, title=False, **kwargs):

    matplotlib.rcParams.update(matplotlib.rcParamsDefault)
    plt.rcParams["font.family"] = "serif"
    x_values = np.linspace(0, 1, 1001)
    number_of_results = 100

    edge_of_bound = (1. - confidence_interval) / 2.
    lower = binom.ppf(1 - edge_of_bound, number_of_results, x_values) / number_of_results
    upper = binom.ppf(edge_of_bound, number_of_results, x_values) / number_of_results
    lower[0] = 0
    upper[0] = 0

    matplotlib.rc('text', usetex=True)
    fig, ax = plt.subplots()
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    ax.fill_between(x_values, lower, upper, alpha=confidence_interval_alpha, color='k')
    for pp, nlive in zip(pps, nlives):
        plt.plot(x_values, pp, label="{} nlive".format(nlive), **kwargs)

    if title:
        ax.set_title("{} {} {}".format(sampler, covariance, modality))
    ax.set_xlabel("C.I.", fontsize=17)#, fontweight="ultralight")
    ax.set_ylabel("Fraction of events in C.I.", fontsize=17)#, fontweight="ultralight")
    ax.legend(handlelength=2, labelspacing=0.15, fontsize=legend_fontsize)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    fig.tight_layout()
    fig.savefig(filename, dpi=500)
    plt.clf()


@bilby.core.utils.latex_plot_format
def get_gaussian_percentile(x, mu, sigma):
    return norm.cdf(x, loc=mu, scale=sigma)


@bilby.core.utils.latex_plot_format
def plot_summary(nlives, means, stds, sampler, covariance, modality, analytical_log_evidence):
    matplotlib.rcParams.update(matplotlib.rcParamsOrig)
    plt.plot(nlives, means, color='blue', label='Mean recovered')
    plt.errorbar(nlives, means, stds, color='blue')
    plt.xlabel('nlive')
    plt.ylabel('ln Z')
    plt.semilogx()
    plt.axhline(analytical_log_evidence, color='red', label='analytical')
    plt.legend()
    plt.savefig("plots/{}_{}_{}_summary.png".format(sampler, covariance, modality))
    plt.clf()


@bilby.core.utils.latex_plot_format
def plot_combined_summary(samplers, covariance, modality):
    graphs = []
    for sampler in samplers:
        try:
            graphs.append(np.loadtxt('{}_{}_{}.txt'.format(sampler, covariance, modality)))
        except Exception:
            continue
    for i in range(len(graphs)):
        graph = graphs[i]
        nlives = graph[0]
        means = graph[1]
        stds = graph[2]
        plt.errorbar(nlives, means, stds, label=samplers[i].replace('_', ' '))
    log_prior_vol = np.sum(np.log([40 for _ in range(15)]))
    analytical_log_evidence = -log_prior_vol
    plt.xlabel('nlive')
    plt.ylabel('ln Z')
    plt.semilogx()
    plt.axhline(analytical_log_evidence, color='red', label='analytical')
    plt.title(modality)
    plt.legend()
    plt.tight_layout()
    plt.savefig("plots/{}_{}_{}_summary.pdf".format('all_samplers', covariance, modality))
    plt.clf()
