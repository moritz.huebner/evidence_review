from plot import plot_combined_summary

covariance = 'review'
samplers = ['cpnest', 'dynesty', 'dynamic_dynesty', 'nestle', 'pymultinest', 'pypolychord']

for modality in ['unimodal', 'bimodal']:
    plot_combined_summary(samplers, covariance, modality)
