#!/usr/bin/env bash
for modality in unimodal bimodal
do
  for nlive in 32 64 128 256 512 1024 2048 4096
  do
    for run_id in {0..99}
    do
      sbatch submit.sh dynesty review ${modality} ${nlive} ${run_id}
    done
  done
done