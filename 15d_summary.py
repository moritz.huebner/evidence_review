import bilby
import numpy as np
import argparse
import os
import matplotlib.pyplot as plt

from plot import evidence_pp_plot, plot_summary, multiple_evidence_pp_plot

logger = bilby.core.utils.logger

parser = argparse.ArgumentParser(description='Settings for the evidence review')
parser.add_argument('--covariance', type=str, nargs=1, help="Either 'review' or 'ones'",
                    choices=['review', 'ones'], default='review')
parser.add_argument('--modality', type=str, nargs=1, help="Either 'unimodal' or 'bimodal'",
                    choices=['unimodal', 'bimodal'])
parser.add_argument('--sampler', type=str, nargs=1, help="Specify the sampler by name",
                    choices=list(bilby.core.sampler.IMPLEMENTED_SAMPLERS.keys()))
parser.add_argument('--njobs', type=int, nargs=1, help="Number of jobs that were run", default=100)
args = parser.parse_args()
sampler = args.sampler[0]
modality = args.modality[0]
covariance = args.covariance[0]
njobs = args.njobs

review_covariance_matrix = np.loadtxt('covariance_matrix.txt')

outdir_base = "outdir/{}/{}_matrix/{}/".format(sampler, covariance, modality)
nlive_dirs = [int(f.path.split('/')[-1]) for f in os.scandir(outdir_base) if f.is_dir()]
nlives = [int(dirname) for dirname in nlive_dirs]
nlives = sorted(nlives)
dim = 15

means = []
stds = []
sampler_quoted_errors = []
pps = []
analytical_log_evidence = np.nan

expected_sigmas = np.sqrt(np.diag(review_covariance_matrix))

for nlive in nlives:
    res_list = bilby.result.ResultList([])
    run_id = 0
    for run_id in range(njobs):
        try:
            res_list.append(bilby.result.read_in_result("outdir/{}/{}_matrix/{}/{}/evidence_review_{}_result.json"
                                                        .format(sampler, covariance, modality, nlive, run_id)))
        except OSError as e:
            logger.warning(e)
    if len(res_list) == 0:
        continue

    for res in res_list:
        priors = res.priors
        log_prior_vol = np.sum(np.log([prior.maximum - prior.minimum for key, prior in priors.items()]))
        analytical_log_evidence = -log_prior_vol

    means.append(np.mean([res.log_evidence for res in res_list]))
    stds.append(np.std([res.log_evidence for res in res_list]))
    logger.info('-------------------')
    logger.info('Modality: {}'.format(modality))
    logger.info('Live points: {}'.format(nlive))
    logger.info('')
    logger.info('Analytic log evidence: {}'.format(-log_prior_vol))
    logger.info('Mean log_evidence: {}'.format(means[-1]))
    logger.info('Empiric standard deviation: {}'.format(stds[-1]))
    logger.info(
        'Mean {} quoted standard deviation: {}'.format(sampler, np.mean([res.log_evidence_err for res in res_list])))
    logger.info('')
    combined = res_list.combine()
    logger.info('Combined log evidence: {}'.format(combined.log_evidence))
    logger.info('Combined {} quoted standard deviation: {}'.format(sampler, combined.log_evidence_err))
    pps.append(evidence_pp_plot(res_list, analytical_log_evidence=analytical_log_evidence,
                                confidence_interval=0.95, filename="plots/{}_{}_{}_{}".format(sampler, covariance, modality, nlive)))

    measured_sigmas = np.zeros((dim, len(res_list)))
    for i in range(dim):
        for j, res in enumerate(res_list):
            measured_sigmas[i][j] = np.std(res.posterior['x{0}'.format(i)])
    mean_sigmas = np.mean(measured_sigmas, axis=1)
    sigma_sigmas = np.std(measured_sigmas, axis=1)/np.sqrt(njobs)

    plt.errorbar(np.arange(0, 15), expected_sigmas - mean_sigmas, sigma_sigmas, fmt='o', capsize=0.2)
    plt.xlabel("Parameter ID")
    plt.ylabel("Sigma residual")
    plt.xticks(np.arange(0, 15))
    plt.savefig("plots/{}_{}_{}_{}_sigma_residuals.pdf".format(sampler, covariance, modality, nlive))
    plt.clf()

del res_list
del combined

np.savetxt('pps_{}_{}_{}.txt'.format(sampler, covariance, modality), np.array(pps))
multiple_evidence_pp_plot(pps, nlives, sampler, covariance, modality, confidence_interval=0.95,
                          filename="plots/{}_{}_{}_summary.pdf".format(sampler, covariance, modality),
                          legend_fontsize='medium', confidence_interval_alpha=0.1)

plot_summary(nlives=nlives[:len(means)], means=means, stds=stds,
             sampler=sampler, covariance=covariance, modality=modality,
             analytical_log_evidence=analytical_log_evidence)
np.savetxt('{}_{}_{}.txt'.format(sampler, covariance, modality), np.array([nlives[:len(means)], means, stds]))
