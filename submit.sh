#!/usr/bin/env bash

#SBATCH --job-name=15d
#
#SBATCH --time=6:00:00
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=4G
#SBATCH --cpus-per-task=1


srun python 15d_gaussian.py --sampler ${1} --covariance ${2} --modality ${3} --nlive ${4} --run_id ${5}