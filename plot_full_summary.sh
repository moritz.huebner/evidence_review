#!/usr/bin/env bash
for modality in unimodal # bimodal
do
  for sampler in dynesty #cpnest dynesty nestle pypolychord
  do
    python 15d_summary.py --covariance review --sampler ${sampler} --modality ${modality}
  done
done